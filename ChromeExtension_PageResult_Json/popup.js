// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';


chrome.runtime.onMessage.addListener(function (request, sender) {
  if (request.action == "getSource") {
    var emailRegx = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    var content = request.source;
    var matched_content = content.match(emailRegx);
    var result = removeDuplicates(matched_content);
    designPage(result);


  }


});
function removeDuplicates(arr) {
  let unique_array = []
  for (let i = 0; i < arr.length; i++) {
    if (unique_array.indexOf(arr[i]) == -1) {
      unique_array.push(arr[i])
    }
  }
  return unique_array
}

function designPage(emails) {
  var button = document.createElement('input');
  var checkboxAll = document.createElement('input');
  var spanAll = document.createElement('span');
  var labelAll = document.createElement('label');


  spanAll.className = "checkmark";

  labelAll.innerHTML = "Select All";
  labelAll.id = "All";
  labelAll.className = "container";


  checkboxAll.type = "checkbox";
  checkboxAll.value = "chkEmailsAll";
  checkboxAll.name = "chkEmailsAll";
  checkboxAll.id = "chkEmailsAll";

  button.type = "button";
  button.name = "btnName";
  button.id = "btnSubmit";
  button.value = "Submit";


  var newLineOuter = document.createElement("BR");
  document.getElementById("chkAll").appendChild(labelAll);
  document.getElementById("All").appendChild(checkboxAll);
  document.getElementById("All").appendChild(spanAll);
  document.getElementById("checkBoxContainer").appendChild(newLineOuter);

  var elementCheckAll = document.getElementById("chkEmailsAll");
  elementCheckAll.onclick = function () {
    var allInputs = document.getElementsByTagName("input");
    for (var i = 0, max = allInputs.length; i < max; i++) {
      if (allInputs[i].type === 'checkbox') {
        if (allInputs[i].checked != true) {
          checkboxAll.checked = true;
          allInputs[i].checked = true;
        }
        else {
          checkboxAll.checked = false;
          allInputs[i].checked = false;
        }

      }
    }
  }



  emails.forEach(function (email) {

    var textbox = document.createElement('input');
    var checkbox = document.createElement('input');
    var span = document.createElement('span');
    var label = document.createElement('label');
    var newLine = document.createElement("BR");

    var arrEmail = email.split("@");
    textbox.type = "text";
    textbox.name = "txtNames";
    textbox.id = "txt" + email;
    textbox.value = arrEmail[0];

    textbox.setAttribute('data-check', email)

    textbox.onchange = function () {
      var checkId = this.getAttribute('data-check');
      var checkElement = document.getElementById(checkId);
      checkElement.setAttribute('data-name', this.value);
    };

    checkbox.type = "checkbox";
    checkbox.value = email;
    checkbox.name = "chkEmails";
    checkbox.id = email;
    checkbox.setAttribute('data-name', arrEmail[0])

    span.className = "checkmark";

    label.innerHTML = email;
    label.id = "lbl" + email;
    label.className = "container";

    document.getElementById("checkBoxContainer").appendChild(label);
    document.getElementById("lbl" + email).appendChild(checkbox);
    document.getElementById("lbl" + email).appendChild(span);
    document.getElementById("checkBoxContainer").appendChild(textbox);
    document.getElementById("checkBoxContainer").appendChild(newLine);
  });

  document.getElementById("checkBoxContainer").appendChild(newLineOuter);

  document.getElementById("btnDiv").appendChild(button);
  var btnSub = document.getElementById("btnSubmit");


  btnSub.onclick = function () {
    chrome.cookies.get({ url: 'http://localhost/userDetail', name: 'userDetail' },
      function (cookie_UserDetail) {
        if (cookie_UserDetail != null) {
          var result= JSON.parse(cookie_UserDetail.value);
 
          var profilePic=document.createElement('img');
          profilePic.src=  result[3];
          

          
          var lblName=document.createElement('label');
          lblName.innerHTML=  result[1];

          var btnSignout=document.createElement('input');
          btnSignout.type = "button";
          btnSignout.name = "btnName";
          btnSignout.id = "btnSignOut";
          btnSignout.value = "Sign Out";

          document.getElementById('tdProfilePic').appendChild(profilePic);
          document.getElementById('tdName').appendChild(lblName);
          document.getElementById('tdBtn').appendChild(btnSignout);

          // var checkedItems = document.getElementsByName('chkEmails');
          // var selectedEmails = '';
          // var eneteredNames = '';
          // var url = 'www.kbihm.com';
          // var actionType = '';
          // var dataFor = 'phone';
          // var params = '';
          // var UserID = cookie_UserID.value;
          // if (checkedItems.length > 1) {
          //   actionType = 'Bulk';
          //   for (var i = 0; i < checkedItems.length; i++) {
          //     if (checkedItems[i].type == 'checkbox' && checkedItems[i].checked == true) {
          //       selectedEmails += checkedItems[i].value + ',';
          //       eneteredNames += checkedItems[i].getAttribute('data-name') + ',';

          //     }
          //   }
          //   selectedEmails = selectedEmails.substring(0, selectedEmails.length - 1);
          //   eneteredNames = eneteredNames.substring(0, eneteredNames.length - 1);
          // }
          // else {
          //   actionType = 'Single';
          //   selectedEmails = checkedItems[i].value + ',';
          //   eneteredNames = checkedItems[i].getAttribute('data-name');
          // }

          // var params =
          //   '&user_id=' + UserID +
          //   '&email=' + selectedEmails +
          //   '&name=' + eneteredNames +
          //   '&url=' + url

          // var request = new XMLHttpRequest();

          // // Open a new connection, using the GET request on the URL endpoint
          // request.open('post', "https://www.kbihm.com/demo/MVC_WebServices/api.php?action=" + actionType + "&type=" + dataFor, true);
          // request.onload = function () {
          // }
          // // request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
          // request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          // request.send(params);
          // // request.onreadystatechange = function () {
          // //   if (request.readyState === 4) {
          // //     callback(request.response);
          // //     console.log(request.response);
          // //   }
          // // }

        }
        else {
          window.open("https://localhost/SignUp.html");
        }
      })
  }
}

function onWindowLoad() {
  var message = document.querySelector('#message');
  chrome.tabs.executeScript(null, {
    file: "getPagesSource.js"
  }, function () {
    // If you try and inject into an extensions page or the webstore/NTP you'll get an error
    if (chrome.runtime.lastError) {
      message.innerText = 'There was an error injecting script : \n' + chrome.runtime.lastError.message;
    }
  });
}

window.onload = onWindowLoad;