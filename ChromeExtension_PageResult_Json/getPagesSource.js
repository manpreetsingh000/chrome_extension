
// @author Rob W <http://stackoverflow.com/users/938089/rob-w>
// Demo: var serialized_html = DOMtoString(document);

function DOMtoString(document_root) {
   var html='', node = document_root.firstChild;
    while (node) {
        switch (node.nodeType) {
            case node.ELEMENT_NODE:
                html +=node.innerHTML;
                break;
        }
        node = node.nextSibling;
    }
    return html;
}

chrome.runtime.sendMessage({
    action: "getSource",
    source: DOMtoString(document)
});