// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let changeColor = document.getElementById('changeColor');
chrome.storage.sync.get('color', function (data) {
  changeColor.style.backgroundColor = data.color;
  changeColor.setAttribute('value', data.color);
});

chrome.runtime.onMessage.addListener(function (request, sender) {
  if (request.action == "getSource") {
    // var emailRegx = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    // var content = request.source;
    // var matched_content = content.match(emailRegx);
    // var result = removeDuplicates(matched_content);
    // var myJSON = JSON.stringify(result)
    // message.innerText = myJSON;
  }
});

// function removeDuplicates(arr) {
//   let unique_array = []
//   for (let i = 0; i < arr.length; i++) {
//     if (unique_array.indexOf(arr[i]) == -1) {
//       unique_array.push(arr[i])
//     }
//   }
//   return unique_array
// }

function onWindowLoad() {
  var message = document.querySelector('#message');
  chrome.tabs.executeScript(null, {
    file: "getPagesSource.js"
  }, function () {
    // If you try and inject into an extensions page or the webstore/NTP you'll get an error
    // if (chrome.runtime.lastError) {
    //   message.innerText = 'There was an error injecting script : \n' + chrome.runtime.lastError.message;
    // }
  });
}

// chrome.tabs.query({
//   active: true,
//   lastFocusedWindow: true
// }, function (tabs) {
//   // and use that tab to fill in out title and url
//   var tab = tabs[0];
//   console.log(tab.url);
// });

window.onload = onWindowLoad;