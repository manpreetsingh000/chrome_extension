
// @author Rob W <http://stackoverflow.com/users/938089/rob-w>
// Demo: var serialized_html = DOMtoString(document);

function DOMtoString(document_root) {
  
    html='';
    var emailRegx = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
    node = document_root.firstChild;
    while (node) {
        switch (node.nodeType) {
            case node.ELEMENT_NODE:
                var matchedEmails=node.innerHTML.match(emailRegx);
                var uniqueMails=removeDuplicates(matchedEmails);
                 for (let index = 0; index < matchedEmails.length; index++) {
                     if(matchedEmails[index]==uniqueMails[index])
                     {
                       console.log(matchedEmails[index]);
                       node.innerHTML.replace(matchedEmails[index],"<mark>"+ matchedEmails[index] +"</mark>" )
                     }
                 }
                break;
        }

        node = node.nextSibling;
    }
    // var a=0;
   
    function removeDuplicates(arr) {
        let unique_array = []
        for (let i = 0; i < arr.length; i++) {
          if (unique_array.indexOf(arr[i]) == -1) {
            unique_array.push(arr[i])
          }
        }
        return unique_array
      }
    // return html;
}

chrome.runtime.sendMessage({
    action: "getSource",
    source: DOMtoString(document)
});